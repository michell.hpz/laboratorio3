const { When, Then, Given, setDefaultTimeout } = require('cucumber');
const { Builder, By, until } = require('selenium-webdriver');
require('chromedriver');
//setDefaultTimeout(5 * 5000);
const assert = require('assert');

Given('I am on the {string} page', {timeout: 5 * 5000}, async function (string) {
  this.driver = new Builder()
    .forBrowser('chrome')
    .build();
  await this.driver.get(`https://the-internet.herokuapp.com/${string}`);
});

When('I loginn with {string} and {string}', async function (string, string2) {
  this.driver.findElement(By.id("username")).sendKeys(string);
  this.driver.findElement(By.id("password")).sendKeys(string2);
  this.driver.findElement(By.className("radius")).click();
});

Then('I should see a message saying {string}', async function (string) {
  await this.driver.wait(until.elementLocated(By.id("flash")));
  let message = await this.driver.findElement(By.id("flash")).getText();
  assert.equal(string, message.substring(0, message.indexOf("!")+1));
});